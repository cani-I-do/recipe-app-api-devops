data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

resource "aws_instance" "bastion" {
  for_each                    = var.vpcs
  ami                         = data.aws_ami.amazon_linux.id
  iam_instance_profile        = aws_iam_instance_profile.bastion.name
  instance_type               = "t2.micro"
  key_name                    = var.bastion_key_name
  user_data                   = file("./templates/bastion/user-data.sh")
  vpc_security_group_ids      = ["${aws_security_group.bastion[each.key].id}"]
  associate_public_ip_address = true
  subnet_id                   = [for sub in local.vpc_subnet_var : aws_subnet.test[sub.subnet_name].id if sub.subnet_rttable == "public"][0]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion", "cost", "shared", "service", "test")
  )
}

resource "aws_security_group" "bastion" {
  for_each    = var.vpcs
  name        = "${local.prefix}-bastion-security-group"
  description = "Allow access to our bastion server"
  vpc_id      = aws_vpc.test[each.key].id
  ingress {
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["172.16.0.0/12", "193.243.160.10/32"]
  }
  /*
  egress {
    description = "Allow SSH"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["172.16.0.0/12"]
  }
  egress {
    description = "Allow SSH"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["172.16.0.0/12"]
  }
  egress {
    description = "Allow SSH"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [for sub in local.vpc_subnet_var : aws_subnet.test[sub.subnet_name].cidr_block if sub.subnet_rttable == "default"]
  }*/

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion", "cost", "shared", "service", "test")
  )
}

