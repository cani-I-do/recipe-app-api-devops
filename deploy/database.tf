resource "aws_db_subnet_group" "main" {
  for_each   = { for i, b in keys(var.vpcs) : b => { index = i } }
  name       = "${local.prefix}-main"
  subnet_ids = local.priv_sub_id_list[each.value.index]
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main", "cost", "shared", "service", "test")
  )
}

resource "aws_security_group" "rds" {
  for_each    = var.vpcs
  description = "Allow access to the RDS database instance."
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.test[each.key].id

  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    security_groups = [
      aws_security_group.bastion[each.key].id,
      aws_security_group.ecs_service[each.key].id
    ]
  }

  tags = local.common_tags
}

resource "aws_db_instance" "main" {
  for_each                = var.vpcs
  identifier              = "${local.prefix}-db"
  name                    = "recipe"
  allocated_storage       = 20
  storage_type            = "gp2"
  engine                  = "postgres"
  engine_version          = "11.4"
  instance_class          = "db.t2.micro"
  db_subnet_group_name    = aws_db_subnet_group.main[each.key].name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0
  multi_az                = false
  skip_final_snapshot     = true
  vpc_security_group_ids  = [aws_security_group.rds[each.key].id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main", "cost", "shared", "service", "test")
  )
}