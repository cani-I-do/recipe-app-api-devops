resource "aws_lb" "api" {
  for_each           = var.vpcs
  name               = "${local.prefix}-main"
  load_balancer_type = "application"
  subnets            = [for sub in local.vpc_subnet_var : aws_subnet.test[sub.subnet_name].id if sub.subnet_rttable == "public"]

  security_groups = [aws_security_group.lb[each.key].id]

  tags = local.common_tags
}

resource "aws_lb_target_group" "api" {
  for_each    = var.vpcs
  name        = "${local.prefix}-api"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.test[each.key].id
  target_type = "ip"
  port        = 8000

  health_check {
    path = "/admin/login/"
  }
}

resource "aws_lb_listener" "api" {
  for_each          = var.vpcs
  load_balancer_arn = aws_lb.api[each.key].arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api[each.key].arn
  }
}

resource "aws_security_group" "lb" {
  for_each    = var.vpcs
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.test[each.key].id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion", "cost", "shared", "service", "test")
  )
}
