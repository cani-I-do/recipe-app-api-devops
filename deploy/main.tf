terraform {
  backend "s3" {
    bucket               = "terraform-kyo-state"
    key                  = "terraform.tfstate"
    region               = "eu-west-1"
    dynamodb_table       = "terraform-kyo-state"
    encrypt              = true
    workspace_key_prefix = "recipe-app"
  }
}

provider "aws" {
  region  = "eu-west-1"
  version = "~> 3.0"
}

data "aws_region" "current" {}