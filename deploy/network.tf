resource "aws_vpc" "test" {
  for_each   = var.vpcs
  cidr_block = each.value["cidr"]
  tags = merge(each.value["tags"],
    {
      Name = each.key
  })
}

resource "aws_subnet" "test" {
  for_each = {
    for s in local.vpc_subnet_var : s.subnet_name => s
  }
  vpc_id            = aws_vpc.test[each.value.network_name].id
  availability_zone = each.value.subnet_az
  cidr_block        = each.value.subnet_cidr
  tags = merge(each.value.tags,
    {
      Name = each.value.subnet_name
  })
}

resource "aws_flow_log" "aws_flow_log_test" {
  for_each        = var.vpcs
  iam_role_arn    = aws_iam_role.cw_log_group_role.arn
  log_destination = aws_cloudwatch_log_group.cw_test_log_group[each.key].arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.test[each.key].id
}

resource "aws_cloudwatch_log_group" "cw_test_log_group" {
  for_each = var.vpcs
  name     = format("cw_log_group-%s", each.key)
}

resource "aws_iam_role" "cw_log_group_role" {
  name = "cw_test_log_group_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "cw_log_group_role_policy" {
  name = "cw_log_group_role_policy"
  role = aws_iam_role.cw_log_group_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams",
        "logs:DescribeLogGroups",
        "logs:ListTagsLogGroup"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_internet_gateway" "vpc-igw" {
  for_each = var.vpcs
  vpc_id   = aws_vpc.test[each.key].id
  tags = merge(each.value["tags"],
    {
      Name = format("vpc-igw-%s", each.key)
  })
}

resource "aws_route_table" "vpc-1-rtb-pub" {
  for_each = var.vpcs
  vpc_id   = aws_vpc.test[each.key].id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.vpc-igw[each.key].id
  }
  tags = merge(each.value["tags"],
    {
      Name = format("pub-rtb-%s", each.key)
  })
}

resource "aws_route_table" "vpc-1-rtb-priv" {
  for_each = {
    for s in local.vpc_subnet_var : s.subnet_name => s... if s.subnet_rttable == "default"
  }
  vpc_id = aws_vpc.test[each.value[0].network_name].id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.public[each.value[0].subnet_az].id
  }
  tags = merge(
    each.value[0].tags,
    local.common_tags,
    map("Name", "${local.prefix}-public-${each.key}")
  )
}

resource "aws_route_table_association" "pub-rt-vpc" {
  for_each = {
    for s in local.vpc_subnet_var : s.subnet_name => s... if s.subnet_rttable == "public"
  }
  subnet_id      = aws_subnet.test[each.value[0].subnet_name].id
  route_table_id = aws_route_table.vpc-1-rtb-pub[each.value[0].network_name].id
}

resource "aws_route_table_association" "priv-rt-vpc" {
  for_each = {
    for s in local.vpc_subnet_var : s.subnet_name => s... if s.subnet_rttable == "default"
  }
  subnet_id      = aws_subnet.test[each.value[0].subnet_name].id
  route_table_id = aws_route_table.vpc-1-rtb-priv[each.key].id
}

resource "aws_eip" "public" {
  for_each = {
    for s in local.vpc_subnet_var : s.subnet_name => s... if s.subnet_rttable == "public"
  }
  vpc = true

  tags = merge(
    each.value[0].tags,
    local.common_tags,
    map("Name", "${local.prefix}-public-${each.key}")
  )
}

resource "aws_nat_gateway" "public" {
  for_each = {
    for s in local.vpc_subnet_var : s.subnet_az => s... if s.subnet_rttable == "public"
  }
  allocation_id = aws_eip.public[each.value[0].subnet_name].id
  subnet_id     = aws_subnet.test[each.value[0].subnet_name].id

  tags = merge(
    each.value[0].tags,
    local.common_tags,
    map("Name", "${local.prefix}-public-${each.key}")
  )
}
