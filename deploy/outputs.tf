output "priv_sub" {
  value = local.priv_sub_id_list
}

output "bastion_private_ip" {
  value = [for private_ip in aws_instance.bastion : private_ip.private_ip]
}

output "bastion_public_ip" {
  value = [for public_dns in aws_instance.bastion : public_dns.public_dns]
}

output "db_host" {
  value = [for db in aws_db_instance.main : db.address]

}

output "api_endpoint" {
  value = [for end in aws_lb.api : end.dns_name]

}
