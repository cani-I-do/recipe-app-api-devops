variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
  default     = ""
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
  default     = ""
}

variable "bastion_key_name" {
  default = "test"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "627815875538.dkr.ecr.eu-west-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "627815875538.dkr.ecr.eu-west-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable vpcs {
  type = map
  default = {
    "tftest" = {
      "cidr" = "10.1.0.0/16",
      "tags" = {
        "cost"    = "shared",
        "service" = "TEST"
      }
    }
  }
}

data "aws_availability_zones" "azs" {
  state = "available"
}

variable subnets {
  type = map
  default = {
    "pub1"  = { "subnet" = 0, "rttable" = "public" },
    "pub2"  = { "subnet" = 1, "rttable" = "public" },
    "priv1" = { "subnet" = 2, "rttable" = "default" },
    "priv2" = { "subnet" = 3, "rttable" = "default" }
  }
}

locals {
  vpc_subnet_var = flatten([
    for v in keys(var.vpcs) : [
      for s in keys(var.subnets) : {
        network_name   = v
        tags           = var.vpcs[v]["tags"]
        subnet_name    = format("%v_%s", v, s)
        subnet_cidr    = cidrsubnet(var.vpcs[v]["cidr"], 8, var.subnets[s]["subnet"])
        subnet_az      = element(local.my_azs, var.subnets[s]["subnet"] % 2)
        subnet_rttable = var.subnets[s]["rttable"]
      }
    ]
  ])
  prefix = "${var.prefix}-${terraform.workspace}-tf"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    ManagedBy   = "Terraform"
  }
  my_azs = slice(data.aws_availability_zones.azs.names, 0, 2)
  priv_sub_id_list = [
    for v in keys(var.vpcs) : [
      for s in local.vpc_subnet_var :
      aws_subnet.test[s.subnet_name].id if s.subnet_rttable == "default" && v == s.network_name
    ]
  ]
}